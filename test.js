// ---------------------------------------------------------------------------
//  test.js
// ---------------------------------------------------------------------------

    var objectKeys = require('./index.js');
    var assert = require('assert');

    describe('object-keys', function () {
        var object = {
            key: 3,
            foo: 5,
            test: 2
        };

        it('should return the keys in order', function () {
            assert.deepEqual(['key', 'foo', 'test'], objectKeys(object));
        });

        it('should return empty array when the object is empty', function () {
            assert.deepEqual([], objectKeys({}));
        });
    });
